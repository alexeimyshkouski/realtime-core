import compose from 'koa-compose';

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

class Composed {
  constructor() {
    _defineProperty(this, "_middleware", []);

    this._composedMiddleware = compose(this._middleware);
  }

  use(fn) {
    const middleware = this._middleware;
    middleware.push(fn.bind(this));
    this._composedMiddleware = compose(middleware);
    return this;
  }

  push(ctx, next) {
    return this._composedMiddleware(ctx, next).then(() => ctx);
  }

  middleware() {
    return this.push.bind(this);
  }

  get length() {
    return this._middleware.length;
  }

}

export { Composed as C, _defineProperty as _ };
