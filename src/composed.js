import compose from 'koa-compose'

export default class Composed {
    _middleware = []

    constructor() {
        this._composedMiddleware = compose(this._middleware)
    }

    use(fn) {
        const middleware = this._middleware
        middleware.push(fn.bind(this))
        this._composedMiddleware = compose(middleware)

        return this
    }

    push(ctx, next) {
        return this._composedMiddleware(ctx, next).then(() => ctx)
    }

    middleware() {
        return this.push.bind(this)
    }

    get length() {
        return this._middleware.length
    }
}