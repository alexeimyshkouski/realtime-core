export const withRequest = Context => {
    return class extends Context {
        get request() {
            return this._context.request
        }

        get socket() {
            return this._context.socket
        }

        get method() {
            return this.request.method
        }

        get url() {
            return this.request.url
        }

        get headers() {
            return this.request.headers
        }
    }
}

export const withMessage = Context => {
    return class extends Context {
        get message() {
            return this._context.message
        }

        set message(value) {
            this._context.message = value
        }

        serialize(value) {
            return value
        }

        deserialize(string) {
            return string
        }
    }
}
export const withConnected = Context => {
    return class extends Context {
        get websocket() {
            return this._context.websocket
        }

        get connection() {
            return this._context.websocket
        }

        get statusCode() {
            return this._context.statusCode
        }

        send(message) {
            message = this.serialize(message)
            return this.websocket.send(message)
        }
    }
}
