import { withRequest, withConnected, withMessage } from './decorators'
import { ContextError, AssertionError } from './errors.js'
// const INSPECT_SYMBOL = Symbol.for('nodejs.util.inspect.custom')

export class Context {
    constructor(ctx, proto = {}) {
        Object.defineProperty(this, '_context', {
            value: Object.assign({}, ctx)
        })

        Object.assign(this, proto)
    }

    assert(condition, code, message) {
        if (!condition) {
            this.throw(new AssertionError(code, message))
        }
    }

    ['throw'](err) {
        if (err instanceof ContextError) {
            throw err
        }

        const { code, message } = err

        throw new ContextError(code, message)
    }

    get app() {
        return this._context.app
    }

    get connected() {
        return this.app.connected
    }
}

@withRequest
class UpgradeContext extends Context { }
export { UpgradeContext }

@withRequest @withConnected @withMessage
class ClientMessageContext extends Context { }
export { ClientMessageContext }

@withMessage
class BroadcastContext extends Context { }
export { BroadcastContext }

