import EventEmitter from 'events'
import { Context } from './context'

export default class Node extends EventEmitter {
    static createContext(ctx, props) {
        return new Context(ctx, props)
    }
}