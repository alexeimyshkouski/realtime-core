import Composed from './composed'
import { BroadcastContext, Context } from './context'

export const publisher = Node => {
    return class extends Node {
        static BroadcastContext = BroadcastContext

        static createBroadcastContext(ctx, assign) {
            return new this.BroadcastContext(ctx, assign)
        }

        _onBroadcastMessage = new Composed()
        
        createBroadcastContext(ctx, assign) {
            return this.constructor.createBroadcastContext(ctx, assign)
        }

        broadcast(fn) {
            this._onBroadcastMessage.use(fn)

            return this
        }

        publish(message) {
            const ctx = this.createBroadcastContext({
                app: this,
                message
            })

            this._onBroadcastMessage.push(ctx).then(_broadcast).catch(this._errorHandler)

            return this
        }
    }
}
export const subscriber = Node => {
    return class extends Node {
        static IncomingContext = Context

        static createIncomingContext(ctx, assign) {
            return new this.IncomingContext(ctx, assign)
        }

        _onIncomingMessage = new Composed()

        createIncomingContext(ctx, assign) {
            return this.constructor.createIncomingContext(ctx, assign)
        }

        incoming(fn) {
            this._onIncomingMessage.use(fn)

            return this
        }
    }
}
export const service = Node => {
    return class extends Node {
        static ConnectOfferContext = Context

        static createConnectionContext(ctx, assign) {
            return new this.ConnectOfferContext(ctx, assign)
        }

        _onConnectOffer = new Composed()

        createConnectionContext(ctx, assign) {
            return this.constructor.createConnectionContext(ctx, assign)
        }

        connect(fn) {
            this._onConnectOffer.use(fn)

            return this
        }
    }
}

function _broadcast(ctx) {
    if (ctx.message) {
        for (const peer of ctx.connected) {
            peer.send(ctx.message)
        }
    }

    return ctx
}