import { Context } from './context.mjs';
import EventEmitter from 'events';

class Node extends EventEmitter {
  static createContext(ctx, props) {
    return new Context(ctx, props);
  }

}

export default Node;
