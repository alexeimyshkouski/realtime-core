import path from 'path'

import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'

const external = (id, parentId, isResolved) => {
    if (parentId) {
        if (isResolved) {
            // console.log(/node_modules/.test(id), id)
            return /node_modules/.test(id)
        }

        // console.log(/^[^.]/.test(id), id)
        return /^[^.]/.test(id)
    }

    // console.log(false, id)
    return false
}

const __dist = path.resolve('.')

const formats = new Map([
    ['cjs', {
        dir: __dist,
        entryFileNames: '[name].js',
        chunkFileNames: 'chunks/[hash].js'
    }],
    ['esm', {
        dir: __dist,
        entryFileNames: '[name].mjs',
        chunkFileNames: 'chunks/[hash].mjs',
        preferConst: true
    }]
].map((entry) => {
    entry[1].format = entry[0]
    return entry
}))

export default {
    input: {
        context: 'src/context/index.js',
        roles: 'src/roles.js',
        node: 'src/node.js',
        composed: 'src/composed.js'
    },
    output: [...formats.values()],
    external,
    plugins: [
        resolve(),
        babel({
            presets: [
                ['@babel/preset-env', {
                    targets: ['maintained node versions'],
                    useBuiltIns: 'usage',
                    corejs: 3
                }]
            ],
            plugins: [
                ['@babel/plugin-proposal-decorators', {
                    legacy: true
                }],
                '@babel/plugin-proposal-class-properties'
            ]
        })
    ]
}