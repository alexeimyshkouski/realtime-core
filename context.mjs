const withRequest = Context => {
  return class extends Context {
    get request() {
      return this._context.request;
    }

    get socket() {
      return this._context.socket;
    }

    get method() {
      return this.request.method;
    }

    get url() {
      return this.request.url;
    }

    get headers() {
      return this.request.headers;
    }

  };
};
const withMessage = Context => {
  return class extends Context {
    get message() {
      return this._context.message;
    }

    set message(value) {
      this._context.message = value;
    }

    serialize(value) {
      return value;
    }

    deserialize(string) {
      return string;
    }

  };
};
const withConnected = Context => {
  return class extends Context {
    get websocket() {
      return this._context.websocket;
    }

    get connection() {
      return this._context.websocket;
    }

    get statusCode() {
      return this._context.statusCode;
    }

    send(message) {
      message = this.serialize(message);
      return this.websocket.send(message);
    }

  };
};

class ContextError extends Error {
  constructor(code, message) {
    super(message);
    this.code = code;
  }

}
class AssertionError extends ContextError {
  constructor(code, message) {
    super(code, 'Assertion failed' + message ? ': ' + message : '');
  }

}

var _class, _class2, _class3;

class Context {
  constructor(ctx, proto = {}) {
    Object.defineProperty(this, '_context', {
      value: Object.assign({}, ctx)
    });
    Object.assign(this, proto);
  }

  assert(condition, code, message) {
    if (!condition) {
      this.throw(new AssertionError(code, message));
    }
  }

  ['throw'](err) {
    if (err instanceof ContextError) {
      throw err;
    }

    const {
      code,
      message
    } = err;
    throw new ContextError(code, message);
  }

  get app() {
    return this._context.app;
  }

  get connected() {
    return this.app.connected;
  }

}

let UpgradeContext = withRequest(_class = class UpgradeContext extends Context {}) || _class;

let ClientMessageContext = withRequest(_class2 = withConnected(_class2 = withMessage(_class2 = class ClientMessageContext extends Context {}) || _class2) || _class2) || _class2;

let BroadcastContext = withMessage(_class3 = class BroadcastContext extends Context {}) || _class3;

export { BroadcastContext, ClientMessageContext, Context, UpgradeContext };
