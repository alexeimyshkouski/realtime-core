import { BroadcastContext, Context } from './context.mjs';
import 'core-js/modules/es.array.iterator';
import { _ as _defineProperty, C as Composed } from './chunks/971f3f48.mjs';
import 'koa-compose';

const publisher = Node => {
  var _class, _temp;

  return _temp = _class = class extends Node {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "_onBroadcastMessage", new Composed());
    }

    static createBroadcastContext(ctx, assign) {
      return new this.BroadcastContext(ctx, assign);
    }

    createBroadcastContext(ctx, assign) {
      return this.constructor.createBroadcastContext(ctx, assign);
    }

    broadcast(fn) {
      this._onBroadcastMessage.use(fn);

      return this;
    }

    publish(message) {
      const ctx = this.createBroadcastContext({
        app: this,
        message
      });

      this._onBroadcastMessage.push(ctx).then(_broadcast).catch(this._errorHandler);

      return this;
    }

  }, _defineProperty(_class, "BroadcastContext", BroadcastContext), _temp;
};
const subscriber = Node => {
  var _class2, _temp2;

  return _temp2 = _class2 = class extends Node {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "_onIncomingMessage", new Composed());
    }

    static createIncomingContext(ctx, assign) {
      return new this.IncomingContext(ctx, assign);
    }

    createIncomingContext(ctx, assign) {
      return this.constructor.createIncomingContext(ctx, assign);
    }

    incoming(fn) {
      this._onIncomingMessage.use(fn);

      return this;
    }

  }, _defineProperty(_class2, "IncomingContext", Context), _temp2;
};
const service = Node => {
  var _class3, _temp3;

  return _temp3 = _class3 = class extends Node {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "_onConnectOffer", new Composed());
    }

    static createConnectionContext(ctx, assign) {
      return new this.ConnectOfferContext(ctx, assign);
    }

    createConnectionContext(ctx, assign) {
      return this.constructor.createConnectionContext(ctx, assign);
    }

    connect(fn) {
      this._onConnectOffer.use(fn);

      return this;
    }

  }, _defineProperty(_class3, "ConnectOfferContext", Context), _temp3;
};

function _broadcast(ctx) {
  if (ctx.message) {
    for (const peer of ctx.connected) {
      peer.send(ctx.message);
    }
  }

  return ctx;
}

export { publisher, service, subscriber };
