'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var context = require('./context.js');
var EventEmitter = _interopDefault(require('events'));

class Node extends EventEmitter {
  static createContext(ctx, props) {
    return new context.Context(ctx, props);
  }

}

module.exports = Node;
